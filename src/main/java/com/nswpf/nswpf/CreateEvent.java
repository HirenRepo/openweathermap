package com.nswpf.nswpf;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CreateEvent {
	static FileInputStream fis;
	static FileInputStream fis1;

	public static void main(String[] args) throws IOException, Exception {
		
		System.setProperty("webdriver.chrome.driver", "C:\\Softwares\\New folder\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(driver, 5);
		
		String url = "http://172.31.20.196:4201/login";
		String userName;
		String passWord;
		
		driver.get(url);
		driver.manage().window().maximize();
		System.out.println("Url Open successfully");

		fis = new FileInputStream("C:\\Softwares\\TestData\\TestData1.xlsx");
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		XSSFSheet loginDetails = workbook.getSheetAt(0);
		
		//Login to the application
		WebElement username = driver.findElement(By.id("email"));
		userName = loginDetails.getRow(1).getCell(0).getStringCellValue();
		WebElement password = driver.findElement(By.id("pwd"));
		passWord = loginDetails.getRow(1).getCell(1).getStringCellValue();		
		WebElement loginButton = driver.findElement(By.xpath(".//*[@id='loginarea']/form/button"));
		
		username.sendKeys(userName);
		password.sendKeys(passWord);
		System.out.println("Username and Password entered successfully");
		loginButton.click();
		System.out.println("Application logged in successfully");

		//Navigate to Dashboardscreen
		WebElement dashboardScreen = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='dashboardScreenOne']/div/div[3]/div/div/div[1]/div[2]/div[1]/a/strong")));
		dashboardScreen.click();
		System.out.println("Navigate to Dashboard Screen successfully");

		//enter the record title
		XSSFSheet EventDetails = workbook.getSheetAt(1);
		String recordTitle = EventDetails.getRow(1).getCell(0).getStringCellValue();
		WebElement title = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='eventComponentScreen']/div/div[1]/div[1]/newcops-input/div/input")));
		title.sendKeys(recordTitle);
		System.out.println("Record Title entered successfully");
		
	
		//enter the time 
		String timeReported = EventDetails.getRow(1).getCell(1).getStringCellValue();
		String[] splitTime = timeReported.split(":");
		if (timeReported.contains(":")) {
			splitTime = timeReported.split(":");
		}
		WebElement dateTimefield = driver.findElement((By.xpath(".//*[@id='eventComponentScreen']/div/div[1]/div[2]/newcops-time/div/div/input")));
		dateTimefield.sendKeys(splitTime[0]);
		dateTimefield.sendKeys(splitTime[1]);
		System.out.println("Time entered successfully");

		//enter the date
		String dateReported = EventDetails.getRow(1).getCell(2).getStringCellValue();
		WebElement dateField = driver.findElement((By.xpath(".//*[@id='eventComponentScreen']/div/div[1]/div[3]/newcops-calendar/div[1]/div/p-calendar/span/input")));
		String[] splitdate = dateReported.split("/");
		if (dateReported.contains("/")) {
			splitdate = dateReported.split("/");
		}
		dateField.sendKeys(splitdate[0]);
		dateField.sendKeys(splitdate[1]);
		dateField.sendKeys(splitdate[2]);
		System.out.println("Date entered successfully");

		//Select the organization unit
		String organisationalUnit = EventDetails.getRow(1).getCell(3).getStringCellValue();
		WebElement orgUnit = driver.findElement(By.xpath(".//*[@id='eventComponentScreen']/div/div[2]/div/div/newcops-select/div/div/select"));
		Select select = new Select(orgUnit);
		select.selectByVisibleText(organisationalUnit);		
		System.out.println("Organisational Unit selected successfully");

		//Select CAD job
		String isCADjob = EventDetails.getRow(1).getCell(4).getStringCellValue();
		WebElement isCAD = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='eventComponentScreen']/div/div[2]/div[2]/newcops-toggle-button/div/div/label")));
		if (isCADjob.contains("no")) {
			System.out.println("Button is already disabled");
		} else {
			isCAD.click();
			System.out.println("Button is selected");
		}
		
		//Select Record Template
		String selectaRecordTemplate = EventDetails.getRow(1).getCell(5).getStringCellValue();
		WebElement recordTem = driver.findElement(By.xpath(".//*[@id='eventComponentScreen']/div/div[3]/div/div/newcops-select/div/div/select"));
		Select select1 = new Select(recordTem);
		select1.selectByVisibleText(selectaRecordTemplate);	
		
//		recordTem.sendKeys(selectaRecordTemplate);
		System.out.println("RecordTemplate selected successfully");
		//Select Security classification
		String securityClassification = EventDetails.getRow(1).getCell(6).getStringCellValue();
		WebElement secClassification = driver.findElement(By.xpath(".//*[@id='eventComponentScreen']/div/div[4]/div/div/newcops-select/div/div/select"));
		Select select2 = new Select(secClassification);
		select2.selectByVisibleText(securityClassification);	
		System.out.println("Security Classification selected successfully");

		//Select securityReason
		String SecurityReason = EventDetails.getRow(1).getCell(7).getStringCellValue();
		if (securityClassification.contains("Unclassified")){
          System.out.println("No need to provide security reason");
		}else{
			WebElement secReason = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='eventComponentScreen']/div/div[5]/div/newcops-text-area/div/textarea")));
			secReason.sendKeys(SecurityReason);
			System.out.println("Security Reason entered successfully");
		}
		//scroll the window
		jse.executeScript("window.scrollBy(0,250)", "");

		//click on create button
		WebElement create = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='newEventScreen']/div/div/form/div[2]/div[1]/button")));
		if (create.isEnabled() && create.isDisplayed()) 
		{
			create.click();
			System.out.println("Event created successfully");
		}

		System.out.println("Navigated to Event details Screen successfully");
		
		WebElement lblrecordtitle = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#eventHeaderHolder > div > div.event-header > div.page-header > div.col-lg-12.col-md-12.paddingZero.headingStyleWithBorder > h2")));
		System.out.println(lblrecordtitle.isDisplayed());
		
		//Closing the driver
		
		workbook.close();
		driver.close();
		System.out.println("Close browser successfully");
		driver.quit();
		
	}
}


