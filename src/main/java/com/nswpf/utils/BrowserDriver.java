package com.nswpf.utils;

import java.net.MalformedURLException;
import java.util.logging.Logger;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.remote.UnreachableBrowserException;

public class BrowserDriver {
	
	private static final Logger LOGGER = Logger.getLogger(BrowserDriver.class.getName());
	private static WebDriver mDriver;
	
	public synchronized static WebDriver getCurrentDriver() {
		if (mDriver==null) {
			try {
                mDriver = BrowserFactory.getBrowser();
	        } catch (UnreachableBrowserException e) {
	            mDriver = BrowserFactory.getBrowser();
	        } catch (WebDriverException e) {
	            mDriver = BrowserFactory.getBrowser();        
	        }finally{
	        	Runtime.getRuntime().addShutdownHook(new Thread(new BrowserCleanup()));
	        }
		}
        return mDriver;
    }
	
	public static void loadPage(String url) {
		getCurrentDriver();
        LOGGER.info("Directing browser to: " + url);
        LOGGER.info("try to loadPage [" + url + "]");
        getCurrentDriver().get(url);
	}
	
	
	public static void close() throws MalformedURLException {
        try {
            getCurrentDriver().quit();
            mDriver = null;
            LOGGER.info("closing the browser");
        } catch (UnreachableBrowserException e) {
            LOGGER.info("cannot close browser: unreachable browser");
        }
    }
	
	public static class BrowserCleanup implements Runnable {
        public void run() {
            try {         
				close();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
        }
    }	
	
	public static void closeWindow() throws MalformedURLException {
		mDriver.close();
	}

}
