package com.nswpf.utils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import java.net.URISyntaxException;

import com.nswpf.constants.Browsers;

public class BrowserFactory {

	private static final Logger LOGGER = Logger.getLogger(BrowserFactory.class.getName());

	public static WebDriver getBrowser() {

		Browsers browser;
		WebDriver driver = null;

		// Setting Selenium Mode locally in Eclipse lets you use your local
		// Browser
		// rather than a selenium grid browser
		System.getenv("SELENIUM_MODE");
		if (System.getenv("SELENIUM_MODE") == null) {
			browser = Browsers.REMOTE;
		} else {
			browser = Browsers.browserForName(System.getenv("SELENIUM_MODE"));
		}

		switch (browser) {
		case CHROME:
			driver = createChromeDriver();
			break;
		case FIREFOX:
			driver = createFirefoxDriver(getFirefoxProfile());
			break;
		default:
			LOGGER.info("I'm creating a firefox because browser is " + browser);
			driver = createFirefoxDriver(getFirefoxProfile());
			break;
		}
		addAllBrowserSetup(driver);
		return driver;
	}
	
	private static WebDriver createChromeDriver() {
		/* An environment variable CHOMEDRIVER_BIN needs to be set 
		to the location of chromedriver.exe for local running */
		String chromedriverbin = System.getenv("CHROMEDRIVER_BIN");
		LOGGER.info("CHROMEDRIVER_BIN is "+System.getenv("CHROMEDRIVER_BIN"));
		System.setProperty("webdriver.chrome.driver", chromedriverbin);
	 
		ChromeOptions options = new ChromeOptions();
	   	options.addArguments("--start-maximized");
	   	options.addArguments("--ignore-certificate-errors");
	   	return new ChromeDriver(options);
	}
	
	private static WebDriver createFirefoxDriver(FirefoxProfile firefoxProfile) {
        return new FirefoxDriver(firefoxProfile);
    }
	
	private static FirefoxProfile getFirefoxProfile() {
        FirefoxProfile firefoxProfile = new FirefoxProfile();
        try {
			firefoxProfile.addExtension(FileUtils.getFile("firebug/firebug-1.9.2.xpi"));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e){
        	e.printStackTrace();
        }

        //See http://getfirebug.com/wiki/index.php/Firebug_Preferences
        firefoxProfile.setPreference("extensions.firebug.currentVersion", "1.9.2");  // Avoid startup screen
        firefoxProfile.setPreference("extensions.firebug.script.enableSites", true);
        firefoxProfile.setPreference("extensions.firebug.console.enableSites", true);
        firefoxProfile.setPreference("extensions.firebug.allPagesActivation", true);
        firefoxProfile.setPreference("extensions.firebug.delayLoad", false);
        return firefoxProfile;
    }
	
	private static void addAllBrowserSetup(WebDriver driver) {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

}
