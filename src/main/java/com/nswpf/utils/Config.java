package com.nswpf.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Config {

	public static Properties props = new Properties();
	
	public static void setNSWPF() throws Exception {
		System.out.println("Setting nswpf UAT Environment");
		props.setProperty("environment", "uat");
		props.setProperty("propertiesFile", "nswpf.properties");
	}

	public synchronized static String getOptionalPropValue(String property) throws IOException {
		String result = "";
		String propFileName = Config.props.getProperty("propertiesFile");
		FileInputStream inputStream = new FileInputStream("properties/" + propFileName);
		System.out.println("--------------------" + propFileName);
		try {
			Properties prop = new Properties();
			if (inputStream != null) {

				prop.load(inputStream);
			}
			result = prop.getProperty(property);
			// System.out.println("Got Property: " + property + " which was "
			// +result);
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		} finally {

			inputStream.close();
		}
		return result;
	}

}
