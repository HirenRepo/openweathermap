package com.nswpf.autotest;


import java.io.IOException;
import java.util.logging.Logger;

import org.openqa.selenium.support.PageFactory;

import com.nswpf.containers.HomePageContainer;
import com.nswpf.utils.BrowserDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class HomePageSteps {
	
	private static final Logger LOGGER = Logger.getLogger(LoginSteps.class.getName());
	private static final HomePageContainer HomePageContainer = PageFactory
			.initElements(BrowserDriver.getCurrentDriver(), HomePageContainer.class);
	
	@When("^I click on Event dashboard on Home Screen$")
	public void I_click_on_Event_dashboard_on_Home_Screen() throws InterruptedException, IOException {
	    HomePageContainer.lnkEvent.click();
	    LOGGER.info("Clicking on Event Button");
	}
	
	@Given("^I verify important labels on the homepage$")
	public void I_verify_important_labels_on_the_homepage() throws Throwable {
		HomePageContainer.logo_image.isDisplayed();
	    LOGGER.info("Logo image present on the home page");
	    
	    HomePageContainer.current_weather_widget.isDisplayed();
	    LOGGER.info("Current weather widget present on the home page");
	    
	    HomePageContainer.search_text_box.isDisplayed();
	    LOGGER.info("Search text box present on the home page");
	    
	    HomePageContainer.search_button.isDisplayed();
	    LOGGER.info("Search button present on the home page");
	    
	    HomePageContainer.current_location_link.isDisplayed();
	    LOGGER.info("Current location link present on the home page");
	    
	    HomePageContainer.titlebar_search_city.isDisplayed();
	    LOGGER.info("Search city title bar link present on the home page");
	    
	}
	
    
    @And("^I verify header labels on the homepage$")
	public void I_verify_header_labels_on_the_homepage() throws Throwable {
    	
    	HomePageContainer.weather_header.isDisplayed();
	    LOGGER.info("Weather header present on the home page");
	    
	    HomePageContainer.maps_header.isDisplayed();
	    LOGGER.info("Maps header present on the home page");
	    
	    HomePageContainer.guide_header.isDisplayed();
	    LOGGER.info("Guide header present on the home page");
	    
	    HomePageContainer.API_header.isDisplayed();
	    LOGGER.info("API header present on the home page");
	    
	    HomePageContainer.price_header.isDisplayed();
	    LOGGER.info("Price header present on the home page");
	    
	    HomePageContainer.partners_header.isDisplayed();
	    LOGGER.info("Partners header present on the home page");

	    HomePageContainer.stations_header.isDisplayed();
	    LOGGER.info("Stations header present on the home page");

	    HomePageContainer.widgets_header.isDisplayed();
	    LOGGER.info("Widgets header present on the home page");

	    HomePageContainer.blog_header.isDisplayed();
	    LOGGER.info("Blog header present on the home page");
	    
    }
    
    
    @And("^I verify footer labels on the homepage$")
	public void I_verify_footer_labels_on_the_homepage() throws Throwable {
    
	    HomePageContainer.weather_city_footer.isDisplayed();
	    LOGGER.info("Weather in your city footer present on the home page");
	    
	    HomePageContainer.maps_layers_footer.isDisplayed();
	    LOGGER.info("Maps layers footer present on the home page");
	    
	    HomePageContainer.weather_apis_footer.isDisplayed();
	    LOGGER.info("Weather APIs footer present on the home page");
	    
	    HomePageContainer.subscribe_footer.isDisplayed();
	    LOGGER.info("Subscribe footer present on the home page");

	    HomePageContainer.station_network_footer.isDisplayed();
	    LOGGER.info("Stations Network footer present on the home page");

	    HomePageContainer.about_footer.isDisplayed();
	    LOGGER.info("About footer present on the home page");

	    HomePageContainer.go_social_footer.isDisplayed();
	    LOGGER.info("Go Social footer present on the home page");

    }
}
