package com.nswpf.autotest;

import java.io.IOException;
import java.util.logging.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.PageFactory;

import com.nswpf.containers.LoginPageContainer;
import com.nswpf.containers.HomePageContainer;
import com.nswpf.utils.*;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginSteps {

	private static final Logger LOGGER = Logger.getLogger(LoginSteps.class.getName());
	private static final LoginPageContainer loginPageContainer = PageFactory
			.initElements(BrowserDriver.getCurrentDriver(), LoginPageContainer.class);
	private static final HomePageContainer HomePageContainer = PageFactory
			.initElements(BrowserDriver.getCurrentDriver(), HomePageContainer.class);

	String frontendurl = null;
	private static final String type= "valid";

	@Given("^I Navigate to (.+) page$")
	public void navigate_to(String page) throws InterruptedException, IOException {
		LOGGER.info("Entering: I navigate to " + page);
		 {
			frontendurl = Config.getOptionalPropValue("nswpf.url");
			System.out.println("----------------------------" + frontendurl);
			BrowserDriver.loadPage(frontendurl);
		}

	}

	@When("^I try to login with (.+) credentials$")
	public void login_with_valid_credentials(String type) throws InterruptedException, IOException {
		if (type.contains("valid")) {
			String username = Config.getOptionalPropValue("nswpf.validUsername");
			String password = Config.getOptionalPropValue("nswpf.validPassword");
			loginPageContainer.txtUsername.sendKeys(username);
			loginPageContainer.txtPassword.sendKeys(password);
			LOGGER.info("Credentials are entered sucessfully");
			loginPageContainer.btnLogin.click();
		}
	}
	
	@Then("^I should see that I logged in (.+)$")
	public void logged_in_status(String success) throws InterruptedException, IOException {

		if (success.contains("successfully")) {
			assert HomePageContainer.lnkEvent.isDisplayed();
			LOGGER.info("Event Dashboard displayed sucessfully");
		}
    
	}
	
	@Given("^I login to NSWPF application$")
	public void login_to_NSWPF_application() throws InterruptedException, IOException {
		login_with_valid_credentials(type);
	}

	

}
