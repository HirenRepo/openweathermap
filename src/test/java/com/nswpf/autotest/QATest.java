package com.nswpf.autotest;

import org.junit.runner.RunWith;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@Cucumber.Options(
					features = "src/test/resources" ,//path to the features	
					tags= {"@common", "@Test3"}
				 )

public class QATest {}
