package com.nswpf.autotest;

import com.nswpf.utils.Config;
import com.nswpf.utils.BrowserFactory;
import cucumber.api.java.Before;

public class Setup {
	
	@Before("@common")
	public static void setNSWPF() throws Exception {
		Config.setNSWPF();	
	}

}
