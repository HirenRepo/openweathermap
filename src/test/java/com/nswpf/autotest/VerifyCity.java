package com.nswpf.autotest;


import java.io.IOException;
import java.util.logging.Logger;

import org.openqa.selenium.support.PageFactory;

import com.nswpf.containers.HomePageContainer;
import com.nswpf.containers.*;
import com.nswpf.utils.BrowserDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class VerifyCity {
	
	private static final Logger LOGGER = Logger.getLogger(LoginSteps.class.getName());
	private static final HomePageContainer HomePageContainer = PageFactory
			.initElements(BrowserDriver.getCurrentDriver(), HomePageContainer.class);
	private static final VerifyCityContainer VerifyCityContainer = PageFactory
			.initElements(BrowserDriver.getCurrentDriver(), VerifyCityContainer.class);
	
	@Given("^I enter city name as \"([^\"]*)\" in the search box$")
	public void I_enter_city(String city) throws Throwable {
		
		//enter city name in search text box
		HomePageContainer.search_text_box.sendKeys(city);
		LOGGER.info("Eneterd city name : " + city);
		
	}

	@And("^I click on the search button$")
	public void I_click_on_search_button() throws Throwable {

		//click on search button
		HomePageContainer.search_button.click();
		LOGGER.info("Clicked on search button");
	}
	
	
	@Then("^I should get the weather details for the \"([^\"]*)\"$")
	public void I_get_city_weather(String city) throws Throwable {
			
		String text = VerifyCityContainer.not_found_text.getText();
		LOGGER.info("Weather text is : " + text);
		if(text.contains(city))
		{
			LOGGER.info("City weather found for city: "+ city);
		}
		else {LOGGER.info("City weather not found");}
		
	}

	
	@Then("^I should get the error message$")
	public void I_click_on_search_button1() throws Throwable {
		
		String text = VerifyCityContainer.not_found_text.getText();
		LOGGER.info("Error text is : " + text);
		if(text.contains("Not found"))
		{
			LOGGER.info("City not found");
		}
		else
		{
			LOGGER.info("ERROR");
		}
	
	}

	
	
	



}