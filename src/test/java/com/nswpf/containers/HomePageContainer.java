package com.nswpf.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class HomePageContainer {
	
	@FindBy(how = How.XPATH, using = "//*[@id='dashboardScreenOne']/div/div[3]/div/div/div[1]/div[2]/div[1]/a/strong")
	public WebElement lnkEvent;
	
	//Important labels on the homepage
	
	@FindBy(how = How.XPATH, using = "//h2[contains(text(),'Current weather and forecasts in your city')]")
	public WebElement current_weather_widget;
	
	@FindBy(how = How.XPATH, using = "//*[@placeholder='Your city name']")
	public WebElement search_text_box;

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Search')]")
	public WebElement search_button;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Current location')]")
	public WebElement current_location_link;
	
	@FindBy(how = How.XPATH, using = "//*[@id='undefined-sticky-wrapper']/div/div/div/div[1]/a/img")
	public WebElement logo_image;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Weather in your city')]")
	public WebElement titlebar_search_city;
	
	//Header validations
	
	@FindBy(how = How.XPATH, using = "//a[(text()='Weather')]")
	public WebElement weather_header;
	
	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Maps')]")
	public WebElement maps_header;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Guide')]")
	public WebElement guide_header;

	@FindBy(how = How.XPATH, using = "//a[(text()='API')]")
	public WebElement API_header;
	
	@FindBy(how = How.XPATH, using = "//a[(text()='Price')]")
	public WebElement price_header;
	
	@FindBy(how = How.XPATH, using = "//a[(text()='Partners')]")
	public WebElement partners_header;
	
	@FindBy(how = How.XPATH, using = "//a[(text()='Stations')]")
	public WebElement stations_header;
	
	@FindBy(how = How.XPATH, using = "//a[(text()='Widgets')]")
	public WebElement widgets_header;
	
	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Blog')]")
	public WebElement blog_header;

	//Footer validations
	
	@FindBy(how = How.XPATH, using = "//h3[contains(text(),'Weather in your city')]")
	public WebElement weather_city_footer;

	@FindBy(how = How.XPATH, using = "//h3[contains(text(),'Map layers')]")
	public WebElement maps_layers_footer;

	@FindBy(how = How.XPATH, using = "//h3[(text()='Weather APIs')]")
	public WebElement weather_apis_footer;

	@FindBy(how = How.XPATH, using = "//h3[(text()='How to subscribe')]")
	public WebElement subscribe_footer;
	
	@FindBy(how = How.XPATH, using = "//h3[(text()='Weather station network')]")
	public WebElement station_network_footer;
	
	@FindBy(how = How.XPATH, using = "//h3[(text()='About')]")
	public WebElement about_footer;
	
	@FindBy(how = How.XPATH, using = "//h3[(text()='Go Social')]")
	public WebElement go_social_footer;
	
	

	
	
	
}
