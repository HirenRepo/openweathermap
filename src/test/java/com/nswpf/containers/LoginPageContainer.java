package com.nswpf.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginPageContainer {

	
	@FindBy(how = How.ID, using = "email")
	public WebElement txtUsername;
	
	@FindBy(how = How.ID, using = "pwd")
	public WebElement txtPassword;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='loginarea']/form/button")
	public WebElement btnLogin;
	
}
