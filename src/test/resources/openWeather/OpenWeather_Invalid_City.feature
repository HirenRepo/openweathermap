Feature: Second end-to-end test
    As a user on the Open Weather Map site, I want to Enter Invalid City Name and Verify the result

    Background:
		Given I Navigate to 'OpenWeather login' page
							
    @common @Test2
    Scenario Outline: Search Invalid city name for weather information
    		Given I enter city name as "<city>" in the search box
    		And I click on the search button
    		Then I should get the error message
    		
    Examples:
		|city|
		|ABCXYZ|
							

