Feature: First end-to-end test
    As a user on the Open Weather Map site, I want to perform a UIBehaviourTest in Homepage

    Background:
    Given I Navigate to 'OpenWeather login' page

    @common @Test1
    Scenario: 1 Home Page UI verification
        Given I verify important labels on the homepage
        And I verify header labels on the homepage
        And I verify footer labels on the homepage

  