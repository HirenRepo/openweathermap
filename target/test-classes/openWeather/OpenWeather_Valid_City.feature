Feature: Second end-to-end test
    As a user on the Open Weather Map site, I want to Enter Valid City Name and Verifies the result

    Background:
		Given I Navigate to 'OpenWeather login' page

    @common @Test3
    Scenario Outline: Search valid city name for weather information
    		Given I enter city name as "<city>" in the search box
    		And I click on the search button
    		Then I should get the weather details for the "<city>"
    
		Examples:
		|city|
		|Mumbai|